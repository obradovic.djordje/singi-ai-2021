from IPython.display import HTML, FileLink, FileLinks
import json
import base64
import time
import random

class Lavirint:
    def __init__(self, putanja):
        with open(putanja) as fp:
            self.lavirint = json.load(fp)
    @property
    def visina(self):
        return len(self.lavirint)
    
    @property
    def sirina(self):
        return len(self.lavirint[0])
    
    def dobavi_polje(self, x, y):
        return self.lavirint[y][x]

    def dobavi_sa_podlogom(self, podloga):
        rezultat = []
        for i in range(len(self.lavirint)):
            for j in range(len(self.lavirint[i])):
                if self.lavirint[i][j]["podloga"] == podloga:
                    rezultat.append((j, i))
        return rezultat

    def dobavi_sa_objektom(self, objekat):
        rezultat = []
        for i in range(len(self.lavirint)):
            for j in range(len(self.lavirint[i])):
                if self.lavirint[i][j]["objekat"] == objekat:
                    rezultat.append((j, i))
        return rezultat
    
class LavirintAnimacija:
    def __init__(self, tileset, lavirint, rezultat):
        self._ucitaj_tileset(tileset)
        self.lavirint = lavirint
        self.rezultat = [rezultat[0].to_dict(rekurzivno=True), list(map(lambda x: x.to_dict(), rezultat[1]))]
    
    def _ucitaj_tileset(self, tileset):
        with open(tileset + "/tileset.json", encoding="UTF-8") as fp:
            self.tileset = json.load(fp)
            for t in self.tileset.values():
                with open(tileset + "/" + t["slika"], 'rb') as ip:
                    t["slika"] = "data:image/png;base64," + base64.b64encode(ip.read()).decode()
    
    def prikazi(self):
        return self._ucitaj_postojeci()
    
    def _ucitaj_postojeci(self):
        with open(self.lavirint) as fp:
            return self._generisi_prikaz(json.dumps(json.load(fp)), json.dumps(self.rezultat[0]), json.dumps(self.rezultat[1][1:]));
    
    def _generisi_prikaz(self, lavirint, putanja, pretraga):
        self._id = "prikaz_{}_{}".format(int(time.time()), random.randint(0, 10000))
        sablon = '''
<style>
    .toolbar-kontejner {{
         display: flex;
         margin-bottom: 10px;
    }}
    .prikaz-kontejner {{
         display: flex;
    }}
    .vreme {{
        width: 300px;
    }}
</style>
<div class="toolbar-kontejner">
<button id="{prikaz_id}_pusti">▶</button>
<input id="{prikaz_id}_vreme" type="range" min="0" max="100" value="0" step="1" class="vreme">
</div>
<div class="prikaz-kontejner">

<div class="centralni-panel">
    <svg width="640" height="640" id="{prikaz_id}_prikaz">
    <marker id="{prikaz_id}_strelica" viewBox="0 0 10 10" refX="10" refY="5"
        markerWidth="6" markerHeight="6"
        orient="auto-start-reverse">
      <path d="M 0 0 L 10 5 L 0 10 z" />
    </marker>
    <marker id="{prikaz_id}_strelica_2" viewBox="0 0 10 10" refX="10" refY="5"
        markerWidth="6" markerHeight="6"
        orient="auto-start-reverse">
      <path d="M 0 0 L 10 5 L 0 10 z" fill="#00ff00" />
    </marker>
    <g id="{prikaz_id}_maske"></g>
    <g id="{prikaz_id}_polja"></g>
    <g id="{prikaz_id}_pretraga"></g>
    <g id="{prikaz_id}_putanja"></g>
    </svg>
</div>

<script>
(() => {{
    let lavirint = {lavirint};
    let pretraga = {pretraga};
    let putanja = {putanja};
    let tileset = {tileset};
    let interval = null;
    {{
        putanja.length = 1;
        let i = 1;
        if(putanja.prethodno) {{
            let trenutno = putanja;
            while(trenutno.prethodno) {{
                trenutno.prethodno.sledece = trenutno;
                trenutno = trenutno.prethodno;
                i++;
            }}
            putanja = trenutno;
            putanja.length = i;
        }}
    }}
    
    let paleta = document.getElementById("{prikaz_id}_tile-izbor-forma");
    let prikaz = document.getElementById("{prikaz_id}_prikaz");
    let vreme = document.getElementById("{prikaz_id}_vreme");
    let pusti = document.getElementById("{prikaz_id}_pusti");
    vreme.setAttribute("max", pretraga.length + putanja.length);
    for(t in tileset) {{
        let maska = document.createElementNS("http://www.w3.org/2000/svg", "mask");
        let maskaSlika = document.createElementNS("http://www.w3.org/2000/svg", "image");
        
        
        if(tileset[t]["tip"] == "objekat") {{
            maska.setAttribute("id", `{prikaz_id}_maska-objekat:${{t}}`);
            maska.setAttribute("maskUnits", "objectBoundingBox");
            maska.setAttribute("maskContentUnits", "objectBoundingBox");
            maska.setAttribute("x", 0);
            maska.setAttribute("y", 0);
            maska.setAttribute("width", 1);
            maska.setAttribute("height", 1);
            
            maskaSlika.setAttribute("href", tileset[t]["slika"]);
            maskaSlika.setAttribute("x", 0);
            maskaSlika.setAttribute("y", 0);
            maskaSlika.setAttribute("width", 1);
            maskaSlika.setAttribute("height", 1);
            maska.append(maskaSlika);
            prikaz.querySelector("#{prikaz_id}_maske").append(maska);
        }}
    }}
    
    let iscrtajLavirint = () => {{
        for(let r = 0; r < lavirint.length; r++) {{
            for(let c = 0; c < lavirint[r].length; c++) {{
                let grupa = document.createElementNS("http://www.w3.org/2000/svg", "g");
                let slika = document.createElementNS("http://www.w3.org/2000/svg", "image");
                let bojaSlike = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                let slikaObjekta = document.createElementNS("http://www.w3.org/2000/svg", "image");
                let bojaObjekta = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                slika.setAttribute("href", tileset[lavirint[r][c]["podloga"]]["slika"]);
                slika.setAttribute("x", 0)
                slika.setAttribute("y", 0)
                slika.setAttribute("width", 32)
                slika.setAttribute("height", 32)
                
                slikaObjekta.setAttribute("x", 0)
                slikaObjekta.setAttribute("y", 0)
                slikaObjekta.setAttribute("width", 32)
                slikaObjekta.setAttribute("height", 32)
                
                if(lavirint[r][c]["objekat"]) {{
                    slikaObjekta.setAttribute("href", tileset[lavirint[r][c]["objekat"]]["slika"]);
                    if(tileset[lavirint[r][c]["objekat"]]["boja"]){{
                        bojaObjekta.setAttribute("fill", tileset[lavirint[r][c]["objekat"]]["boja"]);
                        bojaObjekta.setAttribute("mask", `url(#{prikaz_id}_maska-objekat:${{lavirint[r][c]["objekat"]}})`);
                    }}
                }}
                
                bojaSlike.setAttribute("x", 0)
                bojaSlike.setAttribute("y", 0)
                bojaSlike.setAttribute("width", 32)
                bojaSlike.setAttribute("height", 32)
                bojaSlike.setAttribute("fill", "#00000000")
                if(tileset[lavirint[r][c]["podloga"]]["boja"]){{
                    bojaSlike.setAttribute("fill", tileset[lavirint[r][c]["podloga"]]["boja"])
                }}
                
                bojaObjekta.setAttribute("x", 0)
                bojaObjekta.setAttribute("y", 0)
                bojaObjekta.setAttribute("width", 32)
                bojaObjekta.setAttribute("height", 32)
                bojaObjekta.setAttribute("fill", "#00000000")
                
                if(tileset[lavirint[r][c]["objekat"]] && tileset[lavirint[r][c]["objekat"]]["boja"]){{
                    bojaObjekta.setAttribute("fill", tileset[lavirint[r][c]["objekat"]]["boja"])
                }}
                
                grupa.setAttribute("transform", `translate(${{32*c}}, ${{32*r}})`);
                grupa.append(slika)
                grupa.append(bojaSlike)
                grupa.append(slikaObjekta)
                grupa.append(bojaObjekta)
                prikaz.querySelector("#{prikaz_id}_polja").append(grupa);
            }}
        }}
    }}
    
    let iscrtajPretragu = () => {{
        for(let p of pretraga) {{
            let title = document.createElementNS("http://www.w3.org/2000/svg", "title");
            let content = {{...p}};
            delete content["prethodno"];
            title.textContent = JSON.stringify(content);
            let linija = document.createElementNS("http://www.w3.org/2000/svg", "line");
            linija.setAttribute("x1", p.prethodno.x*32+16)
            linija.setAttribute("y1", p.prethodno.y*32+16)
            linija.setAttribute("x2", p.x*32+16)
            linija.setAttribute("y2", p.y*32+16)
            linija.setAttribute("stroke", "#000000ff")
            linija.setAttribute("stroke-width", "2")
            linija.setAttribute("stroke-dasharray", "4")
            linija.setAttribute("marker-end", "url(#{prikaz_id}_strelica)")
            linija.append(title);
            prikaz.querySelector("#{prikaz_id}_pretraga").append(linija);
        }}
    }}
    let iscrtajPutanju = () => {{
        let trenutno = putanja;
        while(trenutno.sledece) {{
            let title = document.createElementNS("http://www.w3.org/2000/svg", "title");
            let content = {{...trenutno}};
            delete content["prethodno"];
            delete content["sledece"];
            title.textContent = JSON.stringify(content);
            let linija = document.createElementNS("http://www.w3.org/2000/svg", "line");
            linija.setAttribute("x1", trenutno.x*32+16)
            linija.setAttribute("y1", trenutno.y*32+16)
            linija.setAttribute("x2", trenutno.sledece.x*32+16)
            linija.setAttribute("y2", trenutno.sledece.y*32+16)
            linija.setAttribute("stroke-width", "2")
            linija.setAttribute("stroke", "#00ff00ff")
            linija.setAttribute("marker-end", "url(#{prikaz_id}_strelica_2)")
            linija.append(title);
            prikaz.querySelector("#{prikaz_id}_putanja").append(linija);
            trenutno = trenutno.sledece;
        }}
    }}
    iscrtajLavirint();
    iscrtajPretragu();
    iscrtajPutanju();
    vreme.oninput = function() {{
        elements = [...prikaz.querySelector("#{prikaz_id}_pretraga").children,
                    ...prikaz.querySelector("#{prikaz_id}_putanja").children];
        for(let i = 0; i < vreme.value-1; i++) {{
            elements[i].setAttribute("visibility", "visible")
        }}
        
        for(let i = vreme.value; i < elements.length; i++) {{
            elements[i].setAttribute("visibility", "hidden")
        }}
    }}
    
    pusti.onclick = function() {{
        if(interval) {{
            clearInterval(interval);
            vreme.removeAttribute("disabled");
            interval = null;
        }} else {{
            interval = setInterval(()=>{{
                if(Number(vreme.value) >= Number(vreme.max)) {{
                    vreme.value = 0;
                }}
                vreme.value = Number(vreme.value) + 1;
                vreme.dispatchEvent(new Event("input"));
            }}, 100);
            vreme.setAttribute("disabled", "");
        }}
    }}
    
}})();
</script>
</div>
        '''.format(prikaz_id=self._id, tileset=self.tileset, lavirint=lavirint, pretraga=pretraga, putanja=putanja)
        return HTML(sablon)

class LavirintEditor:
    def __init__(self, tileset): 
        self._ucitaj_tileset(tileset)
    
    def ucitaj_postojeci(self, lavirint):
        with open(lavirint) as fp:
            return self._generisi_editor(json.dumps(json.load(fp)));
    
    def otvori_novi(self, sirina=10, visina=10):
        podrazumevana_podloga = list(self.tileset.keys())[0]
        lavirint = []
        for v in range(visina):
            red = []
            for s in range(sirina):
                red.append({"podloga": podrazumevana_podloga, "objekat": None})
            lavirint.append(red)
        
        return self._generisi_editor(json.dumps(lavirint));
    
    def _ucitaj_tileset(self, tileset):
        with open(tileset + "/tileset.json", encoding="UTF-8") as fp:
            self.tileset = json.load(fp)
            for t in self.tileset.values():
                with open(tileset + "/" + t["slika"], 'rb') as ip:
                    t["slika"] = "data:image/png;base64," + base64.b64encode(ip.read()).decode()
    
    def _generisi_editor(self, lavirint):
        self._id = "editor_{}_{}".format(int(time.time()), random.randint(0, 10000))
        sablon = '''
<style>
    .toolbar-kontejner {{
         display: flex;
         margin-bottom: 10px;
    }}
    .editor-kontejner {{
         display: flex;
    }}
    .levi-panel {{
        height: 640px;
        margin-right: 5px;
        overflow-y: auto;
    }}
    .tile-izbor {{
        display:None;
    }}
    .tile-izbor + span {{
        width: 180px;
        padding-left: 2px;
        display:inline-block;
        height: 36px;
        border: 2px solid white;
    }}
    .tile-izbor + span > svg {{
        display:inline-block;
        vertical-align: middle;
    }}
    .tile-izbor:checked + span {{
        border: 2px solid cornflowerblue;
    }}
</style>
<div class="toolbar-kontejner">
<button id="{editor_id}_sacuvaj">Sačuvaj</button>
</div>
<div class="editor-kontejner">
<div class="levi-panel">
    <form id="{editor_id}_tile-izbor-forma">
<label><input type="radio" name="tile" class="tile-izbor" value=""><span><svg width="32" height="32"><image x="0" y="0" width="32" height="32" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QA/wD/AP+gvaeTAAAGWUlEQVRYhb1We3BU5RX/fffevXfvvrI3+8gum2wiSTYhIEEkUTAzFvFRdECNQ3zWt9aKjA8cbZHaG6dKkzLNDDOdEQRnnCkYTClY21qmClGYUBCjBDAJBAPZbNiQ3ez7mc29/UM2bpKNgRo5f917zu/8fuf7zved+WhcYRNFkdLWvFT0wE1lwdbWVplcafE+3e3/sFjMNUNDQ8fISXITcyULOKO6pd4x236rklPShKDq2NDxIupKid/z1v5ykzH3cSWnpAHA7w92892xb8cKeFRs0v/q102zfxp5mQgqzWaLxToLAFKpFCLh8IctLXWjDACsb/rLb/JMptU8z+fUb2o+7BoOrNwi/jI6U/IPNRx83W4vXUIunrjzA+7TkYH+DQBAPSo26fNMptUFBTab0ZirmTevYplRy62fKfFVG/aXmIzGp3heyQCAJEkIBYN7W5rqYgBAMSlG4JRcTjqBpimwNJs7M/Iy0fGqbbNmWWxpj9t9oT8e8dan/6mtv3/u7PkB9+HRUQkA0O8aGIiGo+/OhPwvGtrW2e32JcB3ey/LMoKBwKfviys8aQwDENk1vHllsv2r1xiaNcYikW2N6588cnlSMql95WNbCiOBvzfeGQKAe8RPZhty9U+reH7sqg95vJ5YIvRGZuaPHkSP/PFQnUbNr1NpNIWJeDwY8IXaQoHAS4JR2F4+x7E0U6L7VPfurc9fV5uZn3UQiaLIyIJjM8uyNp8vUL9x3ROHsuFeb2h4MGHO/ZPRnGe+6NLbbJLd6XQtMZsM1kxxv88fjMbDf5jIkbWAsNL+RFVF+WMcy5ITJztlAMsnYjatWc6NaDQNIcWoKdNPURQKCwvsE/Eer/fQjleXTWpt1kmYTKWODw8PDyeSSTk5knRlwxSX5+x4snrYdu+svYTxtCEW9kvZcAAQj8cRCoS/yBab8gy8/Mbbi8Gy8zXxvm2iKKbS/g9WraL5G6n3rlab7jNVWum0/+sLBhy8UApJN1dmOOUkXn8gEHYPuPfHg96Htou3B6ctIJtt33CHkKPV7rx+YdmyUWeESvpi0JQIYHN5AIAsA/uc+ejwF8uMsYLQ9PgNliQJp0/1HNj6YtWNAJEBgJ4sM9lkgMzfWLumsMCypWpBaTWrYAhr4MGZ1Qh1eQEZUOg4EALMzgliodFJBod8GAwSSaESCLk4gwkhUHKcLa/yy5Mn973XCUxxCNO2ac1yzlKoeuYzo+beG4oLqrRqfhyeUlAQFlkR6vTAczYAPk8NVbEeHC3h7pIe+GJ91Ee9fRikyiW1YKUAgFepGIamqwDsmlSAKIqUFW0Og167Qq/iq3V6bWVxgalEyXE/2CrtHCO0AOLuCDwH+2GqKQAIIPBJPFzxDXr956i9/Q5EVfOkZCo1EosmPk7njhFvqf/5bYJGvfnaq4vzcvUapYKZvjuSJIGixvc5FUrCf+wCQAGcgYe2zDAWOzJgxD+7da6Na+/PH9vF9MfTv/v33sjo6LIz5wZbJUma8koB3x22QCiKdG8zjdGyMNbkw7gkH0RBw9/uHoupw1+5rpFbbsjEj1vmnv90+YqvFXaOeKNqpZIr1Wp49USBUCQG54AHVrOQtYBMYwUliIKCv30QAXdQ6vF5NtWt3bUnEzMlw/aGlUvtFlNjZUXRIpqmEI0l0NF1DgRA9QIHptEeZ7IMfH74xL7lz75/MwHkzNiUjf7bJ91naxyqZncg5uj+1lV2tOMMZTboUF1ZOvXK5YvcE+InTjl7XH2Dd1W29gQnplzSOnZsuPM1nU713NLr51qmWrokyfAFwjAI2nH+frd3uKOz95lVa3e3ZMu7pEG069PuA7ddU9Tsi4TLDIL2KnbCFUmOpNB1ph+FNtO43QlF4oljXb2NtS/sensq7ksqAAD2HDgVKl5U2xz3fKOgKapEyNFoAcB53ovjneewcO5VoOnv6UZSozjy9em/rljd/PwP8f5fD5Idb929WKvl3onGE3MpQnBzTSVY9vuZJkky2o52fh5wJ2+pE1uSM14AAHzw4mI+YTR8dN0Cx0Kb1SCk/bIM/Le9q901FL71kXW7vdPx/Ogn2c7Gu+7Ptxp+O39O0RxCKBzt6GnvHRq847FX/uWePnsGCgCAd95cmWfJUb+r5Biz82yw9vE3P3TOBO9lmQyQP4s/01xu3v8A2XtS6BApI/4AAAAASUVORK5CYII="></image><rect x="0" y="0" width="32" height="32" fill="#00000000"></rect></svg> Uklanjanje</span><br></label>
    </form>
</div>

<div class="centralni-panel">
    <svg width="640" height="640" id="{editor_id}_prikaz">
    <g id="{editor_id}_maske"></g>
    <g id="{editor_id}_polja"></g>
    </svg>
</div>

<script>
(() => {{
    let lavirint = {lavirint};
    let tileset = {tileset};
    let paleta = document.getElementById("{editor_id}_tile-izbor-forma");
    let prikaz = document.getElementById("{editor_id}_prikaz");
    for(t in tileset) {{
        let label = document.createElement("label");
        let input = document.createElement("input");
        let span = document.createElement("span");
        let ikonica = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        let slika = document.createElementNS("http://www.w3.org/2000/svg", "image");
        let maska = document.createElementNS("http://www.w3.org/2000/svg", "mask");
        let maskaSlika = document.createElementNS("http://www.w3.org/2000/svg", "image");
        let boja = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        
        ikonica.setAttribute("width", 32);
        ikonica.setAttribute("height", 32);
        
        slika.setAttribute("x", 0)
        slika.setAttribute("y", 0)
        slika.setAttribute("width", 32)
        slika.setAttribute("height", 32)
        slika.setAttribute("href", tileset[t]["slika"]);
        boja.setAttribute("x", 0)
        boja.setAttribute("y", 0)
        boja.setAttribute("width", 32)
        boja.setAttribute("height", 32)
        boja.setAttribute("fill", "#00000000");
        
        if(tileset[t]["tip"] == "objekat") {{
            maska.setAttribute("id", `{editor_id}_maska-ikonica:${{t}}`);
            maska.setAttribute("maskUnits", "objectBoundingBox");
            maska.setAttribute("maskContentUnits", "objectBoundingBox");
            maska.setAttribute("x", 0);
            maska.setAttribute("y", 0);
            maska.setAttribute("width", 1);
            maska.setAttribute("height", 1);
            
            let maskaObjekta = maska.cloneNode();
            maskaObjekta.setAttribute("id", `{editor_id}_maska-objekat:${{t}}`);
            
            maskaSlika.setAttribute("href", tileset[t]["slika"]);
            maskaSlika.setAttribute("x", 0);
            maskaSlika.setAttribute("y", 0);
            maskaSlika.setAttribute("width", 1);
            maskaSlika.setAttribute("height", 1);
            maska.append(maskaSlika);
            ikonica.append(maska);
            maskaObjekta.append(maskaSlika.cloneNode());
            prikaz.querySelector("#{editor_id}_maske").append(maskaObjekta);
            boja.setAttribute("mask", `url(#{editor_id}_maska-ikonica:${{t}})`)
        }}
        if(tileset[t]["boja"]){{
            boja.setAttribute("fill", tileset[t]["boja"])
        }}
        
        ikonica.append(slika);
        ikonica.append(boja);
        
        input.setAttribute("type", "radio");
        input.setAttribute("name", "tile");
        input.setAttribute("class", "tile-izbor");
        input.value = t;
        
        span.append(ikonica);
        span.append(" " + tileset[t]["naziv"]);
        label.append(input);
        label.append(span);
        paleta.append(label);
        paleta.append(document.createElement("br"));
    }}
    
    let iscrtajLavirint = () => {{
        for(let r = 0; r < lavirint.length; r++) {{
            for(let c = 0; c < lavirint[r].length; c++) {{
                let grupa = document.createElementNS("http://www.w3.org/2000/svg", "g");
                let slika = document.createElementNS("http://www.w3.org/2000/svg", "image");
                let bojaSlike = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                let slikaObjekta = document.createElementNS("http://www.w3.org/2000/svg", "image");
                let bojaObjekta = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                slika.setAttribute("href", tileset[lavirint[r][c]["podloga"]]["slika"]);
                slika.setAttribute("x", 0)
                slika.setAttribute("y", 0)
                slika.setAttribute("width", 32)
                slika.setAttribute("height", 32)
                
                slikaObjekta.setAttribute("x", 0)
                slikaObjekta.setAttribute("y", 0)
                slikaObjekta.setAttribute("width", 32)
                slikaObjekta.setAttribute("height", 32)
                
                if(lavirint[r][c]["objekat"]) {{
                    slikaObjekta.setAttribute("href", tileset[lavirint[r][c]["objekat"]]["slika"]);
                    if(tileset[lavirint[r][c]["objekat"]]["boja"]){{
                        bojaObjekta.setAttribute("fill", tileset[lavirint[r][c]["objekat"]]["boja"]);
                        bojaObjekta.setAttribute("mask", `url(#{editor_id}_maska-objekat:${{lavirint[r][c]["objekat"]}})`);
                    }}
                }}
                
                bojaSlike.setAttribute("x", 0)
                bojaSlike.setAttribute("y", 0)
                bojaSlike.setAttribute("width", 32)
                bojaSlike.setAttribute("height", 32)
                bojaSlike.setAttribute("fill", "#00000000")
                if(tileset[lavirint[r][c]["podloga"]]["boja"]){{
                    bojaSlike.setAttribute("fill", tileset[lavirint[r][c]["podloga"]]["boja"])
                }}
                
                bojaObjekta.setAttribute("x", 0)
                bojaObjekta.setAttribute("y", 0)
                bojaObjekta.setAttribute("width", 32)
                bojaObjekta.setAttribute("height", 32)
                bojaObjekta.setAttribute("fill", "#00000000")
                
                if(tileset[lavirint[r][c]["objekat"]] && tileset[lavirint[r][c]["objekat"]]["boja"]){{
                    bojaObjekta.setAttribute("fill", tileset[lavirint[r][c]["objekat"]]["boja"])
                }}
                
                grupa.setAttribute("transform", `translate(${{32*c}}, ${{32*r}})`);
                grupa.append(slika)
                grupa.append(bojaSlike)
                grupa.append(slikaObjekta)
                grupa.append(bojaObjekta)
                prikaz.querySelector("#{editor_id}_polja").append(grupa);
            }}
        }}
    }}
    
    let azurirajLavirint = (x, y, polje) => {{
        let podloga = polje["podloga"];
        let objekat = polje["objekat"];
        let prikazPolja = prikaz.querySelector("#{editor_id}_polja").children[x+y*lavirint.length];
        let slike = prikazPolja.querySelectorAll("image");
        let boje = prikazPolja.querySelectorAll("rect");
        slike[0].setAttribute("href", tileset[podloga]["slika"]);
        boje[0].setAttribute("fill", "#00000000");
        if(tileset[podloga]["boja"]) {{
            boje[0].setAttribute("fill", tileset[podloga]["boja"]);
        }}
        slike[1].setAttribute("href", "")
        boje[1].setAttribute("fill", "#00000000");
        if(objekat) {{
            slike[1].setAttribute("href", tileset[objekat]["slika"]);
            if(tileset[objekat]["boja"]) {{
                boje[1].setAttribute("fill", tileset[objekat]["boja"]);
                boje[1].setAttribute("mask", `url(#{editor_id}_maska-objekat:${{objekat}})`);
            }}
        }}
    }}
    
    iscrtajLavirint();
    
    let crtaj = (event) => {{
        let odabran = document.querySelector('#{editor_id}_tile-izbor-forma > label > input[name="tile"]:checked');
        if(!odabran) {{
            return;
        }}
        let r = Math.floor(event.offsetY/32);
        let c = Math.floor(event.offsetX/32);
        if(r >= lavirint.length || c >= lavirint[r].length) {{
            return;
        }}
        let prethodnaVrednost = lavirint[r][c];
        if(odabran.value && odabran.value != "") {{
            if(tileset[odabran.value]["tip"] == "podloga") {{
                lavirint[r][c]["podloga"] = odabran.value;
            }} else {{
                lavirint[r][c]["objekat"] = odabran.value;
            }}
        }} else {{
            lavirint[r][c]["objekat"] = null;
        }}

        
        azurirajLavirint(c, r, lavirint[r][c])
    }}
    
    prikaz.onmousemove = (event) => {{
        if(event.buttons == 1) {{
            crtaj(event);
        }}
    }}
    
    prikaz.onclick = crtaj;
    
    let sacuvaj = () => {{
        let podaci = "data:application/json;charset=utf-8," + encodeURIComponent(JSON.stringify(lavirint));
        let link = document.createElement("a");
        link.href = podaci;
        link.target = "_blank";
        link.download = `lavirint.json`;
        link.click();
        delete link;
    }}
    
    document.querySelector("#{editor_id}_sacuvaj").onclick = sacuvaj;
}})();
</script>
</div>
        '''.format(editor_id=self._id, tileset=self.tileset, lavirint=lavirint)
        return HTML(sablon)